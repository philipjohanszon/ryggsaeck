const express = require('express');
//var server = express.createServer();
// express.createServer()  is deprecated. 
const server = express(); // better instead

server.use(express.static(__dirname + '/public'));


server.listen(3000);