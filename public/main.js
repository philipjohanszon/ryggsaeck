var Backpack = /** @class */ (function () {
    function Backpack(allItems) {
        this.maxWeight = 60;
        this.maxVolume = 50;
        this.items = allItems;
    }
    //Returnar true om det fungerade annars false
    Backpack.prototype.AddItem = function (id) {
        if (!(this.CurrentWeight() + this.items[id].weight > this.maxWeight) &&
            !(this.CurrentVolume() + this.items[id].volume > this.maxVolume)) {
            this.items[id].holding += 1;
        }
        else {
            alert("För tungt eller för stor volym");
        }
    };
    //Returnar true om det fungerade annars false
    Backpack.prototype.RemoveItem = function (id, amount) {
        if (this.items[id].holding - amount >= 0) {
            this.items[id].holding -= amount;
            return true;
        }
        else {
            return false;
        }
    };
    Backpack.prototype.GetItems = function () {
        var returnItems = [];
        for (var i = 0; i < this.items.length; i++) {
            if (this.items[i].holding > 0) {
                returnItems.push(this.items[i]);
            }
        }
        return returnItems;
    };
    Backpack.prototype.ShowItems = function () {
        var items = this.GetItems();
        var table = document.getElementById("backpackTable");
        //Tömmer hela table:et
        table.innerHTML = "";
        for (var i = 0; i < items.length; i++) {
            var tr = document.createElement("tr");
            for (var j = 0; j < 6; j++) {
                var tdTemp = document.createElement("td");
                switch (j) {
                    case 0:
                        tdTemp.innerHTML = items[i].name;
                        break;
                    case 1:
                        tdTemp.innerHTML = items[i].holding.toString();
                        break;
                    case 2:
                        tdTemp.innerHTML = items[i].weight.toString() + "kg";
                        break;
                    case 3:
                        tdTemp.innerHTML = items[i].volume.toString() + "l";
                        break;
                    case 4:
                        tdTemp.innerHTML = (items[i].weight * items[i].holding).toString() + "kg";
                        break;
                    case 5:
                        tdTemp.innerHTML = (items[i].volume * items[i].holding).toString() + "l";
                        break;
                }
                tr.appendChild(tdTemp);
            }
            table === null || table === void 0 ? void 0 : table.appendChild(tr);
        }
        document.getElementById("backpackWeight").innerHTML = this.CurrentWeight().toString() + "kg";
        document.getElementById("backpackVolume").innerHTML = this.CurrentVolume().toString() + "l";
    };
    Backpack.prototype.CurrentWeight = function () {
        var weight = 0;
        for (var i = 0; i < this.items.length; i++) {
            if (this.items[i].holding > 0) {
                weight += this.items[i].weight * this.items[i].holding;
            }
        }
        return weight;
    };
    Backpack.prototype.CurrentVolume = function () {
        var volume = 0;
        for (var i = 0; i < this.items.length; i++) {
            if (this.items[i].holding > 0) {
                volume += this.items[i].volume * this.items[i].holding;
            }
        }
        return volume;
    };
    return Backpack;
}());
function init() {
    importItems().then(function (items) {
        console.log(items);
        backpack = new Backpack(items);
        showArticles(items);
    })["catch"](function (err) {
        console.log(err);
    });
}
function importItems() {
    return new Promise(function (resolve, reject) {
        $.ajax({
            type: "Get",
            url: "/items.json",
            dataType: "json",
            success: function (data) {
                resolve(data.items);
            },
            error: function () {
                alert("Json filen hittades inte");
                reject("error");
            }
        });
    });
}
function switchView(page) {
    if (page === 1) {
        backpack.ShowItems();
        document.getElementById("container2").style.display = "none";
        document.getElementById("container1").style.display = "block";
    }
    else if (page === 2) {
        document.getElementById("container1").style.display = "none";
        document.getElementById("container2").style.display = "block";
    }
}
function showArticles(items) {
    var table = document.getElementById("articleTable");
    //Tömmer hela table:et
    table.innerHTML = "";
    var _loop_1 = function (i) {
        var tr = document.createElement("tr");
        var td = [];
        for (var j = 0; j < 5; j++) {
            var tdTemp = document.createElement("td");
            var button = void 0;
            switch (j) {
                case 0:
                    tdTemp.innerHTML = items[i].name;
                    break;
                case 1:
                    tdTemp.innerHTML = items[i].weight.toString() + "kg";
                    break;
                case 2:
                    tdTemp.innerHTML = items[i].volume.toString() + "l";
                    break;
                case 3:
                    button = document.createElement("button");
                    button.className = "btn btn-primary";
                    button.innerHTML = "+";
                    button.onclick = function () { return backpack.AddItem(items[i].id); };
                    tdTemp.appendChild(button);
                    break;
                case 4:
                    button = document.createElement("button");
                    button.className = "btn btn-danger";
                    button.innerHTML = "-";
                    button.onclick = function () { return backpack.RemoveItem(items[i].id, 1); };
                    tdTemp.appendChild(button);
                    break;
            }
            tr.appendChild(tdTemp);
        }
        table === null || table === void 0 ? void 0 : table.appendChild(tr);
    };
    for (var i = 0; i < items.length; i++) {
        _loop_1(i);
    }
}
var backpack;
init();
