
interface Item {
    name: string,
    id: number,
    holding: number,
    weight: number,
    volume: number
}

class Backpack {
    maxWeight: number = 60;
    maxVolume: number = 50;
    items: Item[];

    constructor(allItems: Item[]) {
        this.items = allItems;
    }

    //Returnar true om det fungerade annars false
    AddItem(id: number): void {
        if (!(this.CurrentWeight() + this.items[id].weight > this.maxWeight) &&
            !(this.CurrentVolume() + this.items[id].volume > this.maxVolume)) {
            this.items[id].holding += 1;
        }
        else {
            alert("För tungt eller för stor volym");
        }
    }

    //Returnar true om det fungerade annars false
    RemoveItem(id: number, amount: number): boolean {
        if (this.items[id].holding - amount >= 0) {
            this.items[id].holding -= amount;
            return true;
        }
        else {
            return false;
        }
    }

    GetItems(): Item[] {
        let returnItems: Item[] = [];

        for (let i = 0; i < this.items.length; i++) {
            if (this.items[i].holding > 0) {
                returnItems.push(this.items[i]);
            }
        }

        return returnItems;
    }

    ShowItems(): void {
        let items: Item[] = this.GetItems();

        const table = document.getElementById("backpackTable");
        //Tömmer hela table:et
        table.innerHTML = "";

        for (let i = 0; i < items.length; i++) {
            let tr = document.createElement("tr");

            for (let j = 0; j < 6; j++) {
                let tdTemp = document.createElement("td");

                switch (j) {
                    case 0:
                        tdTemp.innerHTML = items[i].name;
                        break;
                    case 1:
                        tdTemp.innerHTML = items[i].holding.toString();
                        break;
                    case 2:
                        tdTemp.innerHTML = items[i].weight.toString() + "kg";
                        break;
                    case 3:
                        tdTemp.innerHTML = items[i].volume.toString() + "l";
                        break;
                    case 4:
                        tdTemp.innerHTML = (items[i].weight * items[i].holding).toString() + "kg";
                        break;
                    case 5:
                        tdTemp.innerHTML = (items[i].volume * items[i].holding).toString() + "l"
                        break;
                }
                tr.appendChild(tdTemp);
            }

            table?.appendChild(tr);
        }

        document.getElementById("backpackWeight").innerHTML = this.CurrentWeight().toString() + "kg";
        document.getElementById("backpackVolume").innerHTML = this.CurrentVolume().toString() + "l";

    }

    CurrentWeight(): number {
        let weight: number = 0;

        for (let i = 0; i < this.items.length; i++) {
            if (this.items[i].holding > 0) {
                weight += this.items[i].weight * this.items[i].holding;
            }
        }

        return weight;
    }

    CurrentVolume(): number {
        let volume: number = 0;

        for (let i = 0; i < this.items.length; i++) {
            if (this.items[i].holding > 0) {
                volume += this.items[i].volume * this.items[i].holding;
            }
        }

        return volume;
    }

}


function init(): void {
    importItems().then((items) => {
        console.log(items);
        backpack = new Backpack(items);
        showArticles(items);
    }).catch((err) => {
        console.log(err)
    });
}

function importItems(): Promise<Item[]> {
    return new Promise<Item[]>((resolve, reject) => {
        $.ajax({
            type: "Get",
            url: "/items.json",
            dataType: "json",
            success: function (data) {
                resolve(data.items);
            },
            error: function () {
                alert("Json filen hittades inte");
                reject("error");
            }
        });

    });
}

function switchView(page: number): void {
    if (page === 1) {
        backpack.ShowItems();
        document.getElementById("container2").style.display = "none";
        document.getElementById("container1").style.display = "block";
    }
    else if (page === 2) {
        document.getElementById("container1").style.display = "none";
        document.getElementById("container2").style.display = "block";
    }
}

function showArticles(items: Item[]) {

    const table = document.getElementById("articleTable");
    //Tömmer hela table:et
    table.innerHTML = "";

    for (let i = 0; i < items.length; i++) {
        let tr = document.createElement("tr");

        let td = [];

        for (let j = 0; j < 5; j++) {
            let tdTemp = document.createElement("td");
            let button

            switch (j) {
                case 0:
                    tdTemp.innerHTML = items[i].name;
                    break;
                case 1:
                    tdTemp.innerHTML = items[i].weight.toString() + "kg";
                    break;
                case 2:
                    tdTemp.innerHTML = items[i].volume.toString() + "l";
                    break;
                case 3:
                    button = document.createElement("button");
                    button.className = "btn btn-primary";
                    button.innerHTML = "+";
                    button.onclick = () => backpack.AddItem(items[i].id);
                    tdTemp.appendChild(button);
                    break;
                case 4:
                    button = document.createElement("button");
                    button.className = "btn btn-danger";
                    button.innerHTML = "-";
                    button.onclick = () => backpack.RemoveItem(items[i].id, 1);
                    tdTemp.appendChild(button);
                    break;
            }
            tr.appendChild(tdTemp);
        }

        table?.appendChild(tr);
    }
}

let backpack: Backpack;

init();
